variable "custom_tags" {
  type = map(string)
  default = {}
}

locals {
  default_tags = merge(var.custom_tags,{
    Organization = "formation",
    Project = "setup",
    Layer = "network",
    Terraform = true
  })
}

//variable "students" {
//  type = set (object({}))
//  default = []
//}

//variable "ssh_private_key" {
//  type = string
//  default = afaire
//}

variable "instance_type" {
  type = string
  default = "t2.micro"
}

variable "env" {
  type = string
  default = "dev"
}
