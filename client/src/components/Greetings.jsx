import React from 'react';

class Greetings extends React.Component{

    constructor(props){
        super(props)
        this.state = {
            user:{ firstname:null},
            timeline:[]
        };
    }

    componentDidMount(){
        let thisComponent = this;
        fetch('http://localhost:8080/greetings')
        .then(res => res.json())
        .then((result)=>{
            console.log(result['hydra:member'])
            thisComponent.setState({timeline:result['hydra:member']})
        });

    }

    


    render(){

    return (
        <div>
        <section className="ftco-section">
        <div className="container">           
             <p>___________</p>
            <div className="row">
                <div className="col-lg-12">                
               {this.state.timeline.map(post =>{

                   return(<div>
                            <p>{post.name}</p>
                        </div>)})}

              </div>
            </div>
        </div>
       </section>
    </div>
    ); 
   
    }

}

export default Greetings;
