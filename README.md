## Démarrage
test
- API Platform

```
cd api-platform
docker-compose pull (si c'est le premier démarrage)
docker-compose up -d
```

API Platform créera par défaut une table Greeting pour tester une entity.

La documentation de l'API est normalement disponible à l'url [http://localhost:8080/](http://localhost:8080/) 


- React 

```
cd client
npm i (si c'est le premier démarrage & mettre 'sudo')
npm run start
```

L'appli React tourne sur l'adresse  [http://localhost:3000/](http://localhost:3000/) 

